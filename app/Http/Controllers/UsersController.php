<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;

class UsersController extends Controller
{
	public function index(Request $request, Generator $faker) {
	
		$accessToken = "1ebd60d044836d4dfa7959789730eaf23e923c5a";

		$ch = curl_init("http://10.100.64.5:8082/api/1.0/workflow/users");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$aUsers = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		return ["data" => json_decode($aUsers)];
	}	

	public function approve() {

		$accessToken = "1ebd60d044836d4dfa7959789730eaf23e923c5a";

		$ch = curl_init("http://10.100.64.5:8082/api/1.0/workflow/plugin-test1/pass");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		return ["data" => json_decode($aUsers)];
	}
}
